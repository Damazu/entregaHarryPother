import UIKit
import Alamofire
import Kingfisher

struct Harry:Decodable {
    let name:String
    let actor:String
    let image:String
}

class ViewController: UIViewController,UITableViewDataSource {

    var listaDePersonagens: [Harry]=[]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDePersonagens.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as!MyCell
        let personagem = self.listaDePersonagens[indexPath.row]
        
        
        cell.NomePersonagem.text = personagem.name
        cell.NomeAutor.text = personagem.actor
        cell.imgPersonagem.kf.setImage(with: URL(string: personagem.image))
        return cell
    }
    
    @IBOutlet weak var tableViewPersonagens: UITableView!
    
    func getnovoharrypoter(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Harry].self) { response in
            if let personagem = response.value {
                self.listaDePersonagens = personagem
            }
            self.tableViewPersonagens.reloadData()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tableViewPersonagens.dataSource = self
        
        getnovoharrypoter()
    }
}
